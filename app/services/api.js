
const lex = 'https://archeo-lex.fr/durafront/server/diff'

module.exports = {
  postDiff: async (texteAmendement = '', texteArticle = '') => {
    return $.post({
      'type': 'POST',
      'url': lex,
      'crossDomain': true,
      'dataType': 'json',
      'timeout': 120*1000,
      'data': JSON.stringify({
        texteAmendement,
        texteArticle
      })
    })
  }
}