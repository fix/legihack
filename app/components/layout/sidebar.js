
const addMenu = (menu) => {
  if(menu.type === 'group') {
    $("#sidebar-custom").append(`
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse${menu.path}" aria-expanded="true" aria-controls="collapse${menu.path}">
          <i class="fas fa-fw ${menu.icon}"></i>
          <span>${menu.label}</span>
        </a>
        <div id="collapse${menu.path}" class="collapse" aria-labelledby="heading${menu.path}" data-parent="#accordionSidebar">
          <div id="${menu.path}" class="bg-white py-2 collapse-inner rounded">
          </div>
        </div>
      </li>
    `)
  }
  else if(menu.type === 'divider') {
    $("#sidebar-custom").append(`
      <hr class="sidebar-divider my-0">
    `)
  }
  else {
    if(menu.path) {
      $("#sidebar-custom " + menu.path).append(`
        <a class="applink collapse-item" page="${menu.name}" href="#">
        <i class="fas fa-fw ${menu.icon}"></i>
            <span>${menu.label}</span>
        </a>
      `)
    } else {
      $("#sidebar-custom").append(`
        <li class="nav-item">
          <a class="applink nav-link" page="${menu.name}" href="#">
            <i class="fas fa-fw ${menu.icon}"></i>
            <span>${menu.label}</span>
          </a>
        </li>
      `)
    }
    
  }
}

const buildMenu = menuList => {
  menuList.forEach(menu => {
    addMenu(menu)
  })
}

module.exports = {
  name: 'Sidebar',
  content: `
    <ul class="fixed-top navbar-nav bg-gradient-primary sidebar toggled sidebar-dark accordion" id="accordionSidebar">
      <a class="sidebar-brand applink d-flex align-items-center justify-content-center" page="welcome" href="#">
        <i class="fas fa-university fa-3x"></i>
        <div class="sidebar-brand-text mx-3" id='sidebar-brand-title'></div>
      </a>

      <hr class="sidebar-divider my-0"/>

      <div id="sidebar-custom"/>

      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
  `,
  init: () => {
    $('#sidebar-brand-title').text('ANS')
    $('#accordionSidebar').on('click', e => {
      if(e.target.id !== "accordionSidebar") return
      if ($(window).width() < 768) {
        $(".sidebar").addClass("toggled");
        $("body").addClass("sidebar-toggled");
      }
      $('.sidebar .collapse').collapse('hide');
    })

    $("#sidebarToggle").on('click', e => {
      $("body").toggleClass("sidebar-toggled");
      $(".sidebar").toggleClass("toggled");
      if ($(".sidebar").hasClass("toggled")) {
        $('.sidebar .collapse').collapse('hide');
      };
    });
  },
  addMenu,
  buildMenu
}
