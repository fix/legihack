window.$ = require('jquery')
import 'bootstrap'
import './vendor/sbadmin2/scss'
import '@fortawesome/fontawesome-free/css/all.css'
import './vendor/jquery-easing'
import './vendor/sbadmin2/js/sb-admin-2'

const router = require('./services/router')
const pages = require('./components')
const store = require('./services/storage')
const api = require('./services/api')

window.ansapp = require('.')

const createMenu = (name, path) => {
  const page = pages[name]
  return {
    label: page.name,
    name,
    path,
    icon: page.icon
  }
}

const start = async () => {
  const {sidebar, topbar, footer} = pages.layout
  $("#wrapper").append(sidebar.content)
  $("#wrapper").append(topbar.content)
  $("#footer").append(footer.content)
  await sidebar.init()
  await topbar.init()

  const menuList = [
    createMenu('welcome', ''),
    {
      type: 'divider'
    },
    createMenu('search', ''),
    createMenu('settings', '')
  ]
  
  sidebar.buildMenu(menuList)

  $(".applink").click(async event => {
    if ($(window).width() < 768) {
      $(".sidebar").addClass("toggled")
      $("body").addClass("sidebar-toggled")
    } else {
      $("#content").addClass("mb-sidebar")
      $(".topbar").addClass("mb-sidebar")
    }
    $('.sidebar .collapse').collapse('hide')
    
    event.preventDefault()
    await router.loadPage($(event.currentTarget).attr('page'))
  })

  await router.loadPage('welcome')
}



start()