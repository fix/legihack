const store = require('./services/storage')
const crypto = require('crypto')

const modalprompt = (name, type = 'text') => {
  const form = `
  <form>
    <input type="${type}" id="prompt-answer" class="form-control small" placeholder="Enter ${name}" aria-label="${name}">
    <br/>
    <button type="button" class="btn btn-secondary">Cancel</button>
    <button type="button" class="btn btn-primary">OK</button>
  </form>
  `
  return  new Promise(resolve => {
    $('#ans-modal').find('#ans-modal-title').html('prompt')
    $('#ans-modal').find('.modal-body').html(form)
    $('#ans-modal').modal('show')
    $('#ans-modal').find('#prompt-answer').trigger('focus')
    $('#ans-modal').find('button').click(event => {
      $('#ans-modal').modal('hide')
      const result = $('#ans-modal').find('#prompt-answer').val()
      resolve(result)
    })
    $('#ans-modal').find('form').submit(event => {
      event.preventDefault()
      $('#ans-modal').modal('hide')
      const result = $('#ans-modal').find('#prompt-answer').val()
      resolve(result)
    })
  })
}

const alert = (message, level) => {
  const html = `
  <div class="toast" data-delay='5000' role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-body text-${level}">
      ${message}
    </div>
  </div>
  `
  $("#toasts").html(html)
  $("#toasts .toast").toast('show')
} 

const setPersistence = (persistenceTime) => {
  localStorage.setItem('persistence', persistenceTime)
}

const getPersistence = () => {
  return localStorage.getItem('persistence')
}

const saveHashedPassword = (password, repassword) => {
  if(!password) return true
  console.log(password)
  console.log(repassword)
  if(password !== repassword) return false
  const salt = crypto.randomBytes(8).toString('hex')
  const hash = crypto.createHmac('sha512', salt)
  hash.update(password)
  const saltedhash = salt + ":" + hash.digest('hex')
  localStorage.setItem('saltedhash', saltedhash)
  return true
}

const isPasswordEnabled = () => {
  return !!localStorage.getItem('saltedhash')
}

const getPassword = async () => {
  const saltedhash = localStorage.getItem('saltedhash')
  if(!saltedhash) return null
  let password = store.getPersistent('password')
  if(password) return password
  password = await modalprompt('password', 'password')
  if(!password) throw new Error('cancelled')
  const salt = saltedhash.split(":")
  const hash = crypto.createHmac('sha512', salt[0])
  hash.update(password)
  if(hash.digest('hex') === salt[1]){
    store.setPersistent('password', password, localStorage.getItem('persistence'))
    return password
  }
  else throw new Error('wrong password')
}


module.exports = {
  alert,
  modalprompt,
  getPassword,
  saveHashedPassword,
  setPersistence,
  getPersistence
}