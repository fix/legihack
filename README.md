Quickstart
----------
1. install nodejs
2. npm install
3. npm start

open browser at http://localhost:1234

- call to the archeo-lex API: app/services/api
- UI for the API call: app/components/welcome.js

In case of issue with the app, remove the parcel cache and restart:
- rm -fr .cache/
- npm start